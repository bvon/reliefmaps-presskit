Invoke-Expression -Command $(aws ecr get-login --no-include-email --region eu-central-1)
docker build -t reliefmaps-presskit .
docker tag reliefmaps-presskit:latest 774843963874.dkr.ecr.eu-central-1.amazonaws.com/reliefmaps-presskit:latest
docker push 774843963874.dkr.ecr.eu-central-1.amazonaws.com/reliefmaps-presskit:latest